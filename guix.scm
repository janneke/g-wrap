;;; guix.scm
;;;
;;; This file is part of g-wrap.
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2, or (at your option) any later version.
;;; 
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;; 
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this software; see the file COPYING.  If not,
;;; write to the Free Software Foundation, 675 Mass Ave, Cambridge,
;;; MA 02139, USA.

;;; Commentary:
;;
;; GNU Guix development package.  To build and install, run:
;;
;;   guix package -f guix.scm
;;
;; To build it, but not install it, run:
;;
;;   guix build -f guix.scm
;;
;; To use as the basis for a development environment, run:
;;
;;   guix environment -l guix.scm
;;
;;; Code:

(use-modules (guix packages)
             (guix licenses)
             (guix git-download)
             (guix build-system gnu)
             (guix utils)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages gettext)
             (gnu packages guile)
             (gnu packages libffi)
             (gnu packages pkg-config)
             (gnu packages texinfo))

(define-public guile-next-lib
  (package
    (inherit guile-lib)
    (name "guile-next-lib")
    (version "0.2.2")
    (inputs `(("guile" ,guile-next)))
    (arguments
     `(#:tests? #f ;; 2 tests still fail
      ,@(substitute-keyword-arguments (package-arguments guile-lib)
       ((#:phases phases)
       `(modify-phases ,phases
        (replace 'patch-module-dir
                 (lambda _
                   (substitute* "src/Makefile.in"
                     (("^moddir[[:blank:]]*=[[:blank:]]*([[:graph:]]+)" _ rhs)
                      (string-append "moddir = " rhs "/2.2\n"))))))))))))

(define-public g-wrap
  (package
    (name "g-wrap")
    (version "1.9.15")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "git://git.sv.gnu.org/g-wrap.git")
                    (commit "1d8df44")))
              (sha256
               (base32
                "19bwqfk33djd51ijylvcmz3zz9pqvv1k7bk8k912b1bwbpfr3j0y"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("gettext" ,gnu-gettext)
       ("libtool" ,libtool)
       ("pkg-config" ,pkg-config)
       ("texinfo" ,texinfo)))
    (propagated-inputs
     `(("guile" ,guile-next)
       ("guile-lib" ,guile-next-lib)))
    (inputs
     `(("libffi" ,libffi)))
    (arguments
      `(#:configure-flags '("--disable-Werror")
        #:phases
        (modify-phases %standard-phases
          (add-after 'unpack 'autogen.sh
            (lambda _
              (setenv "NOCONFIGURE" "1")
              (zero? (system* "sh" "autogen.sh"))))
          (add-before 'configure 'pre-configure
            (lambda* (#:key outputs #:allow-other-keys)
              (let ((out (assoc-ref outputs "out")))
                (substitute* (find-files "." "^Makefile.in$")
                  (("guilemoduledir =.*guile/site" all)
                   (string-append all "/2.2")))
                #t))))))
    (synopsis "Generate C bindings for Guile")
    (description "G-Wrap is a tool and Guile library for generating function
wrappers for inter-language calls.  It currently only supports generating Guile
wrappers for C functions.  Given a definition of the types and prototypes for
a given C interface, G-Wrap will automatically generate the C code that
provides access to that interface and its types from the Scheme level.")
    (home-page "http://www.nongnu.org/g-wrap/index.html")
    (license lgpl2.1+)))

g-wrap
